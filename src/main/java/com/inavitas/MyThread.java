package com.inavitas;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.time.ZoneOffset;
import java.util.concurrent.ExecutionException;

public class MyThread implements Runnable{
    String name;
    Thread t;
    List<String[]> data;
    String[] headers;
    MyThread (String threadname, List<String[]> data, String[] headers){
        name = threadname;
        t = new Thread(this, name);
        this.data = data;
        this.headers = headers;
        t.start();
    }

    @Override
    public void run() {
        createJSON(data);
    }

    public void createJSON(List<String[]> data){
        ArrayList<JSONObject> deviceMessage = new ArrayList<>();
        JSONObject body; //
        JSONArray dataList;
        JSONObject dataObject;
        for(int i = 0; i< data.size(); i++){
            body = new JSONObject();
            dataList = new JSONArray();
            for (int j = 0; j< data.get(i).length; j++){
                if (j == 0) {
                    body.put("deviceId", data.get(i)[j]);
                } else if (j==1){
                    LocalDateTime date = LocalDateTime.parse(data.get(i)[j].replace(" ", "T"));
                    long timeInSeconds = date.toEpochSecond(ZoneOffset.UTC);

                    body.put("dataTime", timeInSeconds);
                } else {
                    dataObject = new JSONObject();
                    dataObject.put("dataName", headers[j]);
                    dataObject.put("dataValue", data.get(i)[j]);
                    dataList.add(dataObject);
                }
            }
            body.put("dataList", dataList);
            deviceMessage.add(body);

        }
        runProducer(deviceMessage);

    }

    static void runProducer(ArrayList<JSONObject> messageList) {
        KafkaProducer<Long, String> producer = ProducerCreator.createProducer();
        for (int index = 0; index < messageList.size(); index++) {
            ProducerRecord<Long, String> record = new ProducerRecord<Long, String>("sample-data",
                    messageList.get(index).toJSONString());
            try {
                RecordMetadata metadata = producer.send(record).get();
            }
            catch (ExecutionException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            }
            catch (InterruptedException e) {
                System.out.println("Error in sending record");
                System.out.println(e);
            }
        }
    }

    public void printData(List<String[]> data){
        String[] dataRow;
        for (int i = 0; i<data.size(); i++){
            dataRow = data.get(i);
            for (int j = 0; j<dataRow.length; j++){
                System.out.print(dataRow[j] + " ");
            }
            System.out.println();
        }
    }

}
