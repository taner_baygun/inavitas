package com.inavitas;

import java.io.*;
import java.util.ArrayList;

public class CaseStudy {
    public static void readCSV() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader("./src/main/resources/SampleData_.csv"));
        String[] headers = new String[0]; // Column names
        String[] row; // current row
        String rawRow;
        ArrayList<String[]> values = new ArrayList<>(); // Data read from file
        ArrayList<Integer> uniqueIds = new ArrayList<>(); // Unique device ids
        ArrayList<Integer> startIndex = new ArrayList<>(); // Starting index of ids

        int lastId = -1;
        int count = 0;
        try {
            String strCurrentLine;

            for (int i = 0; (strCurrentLine = csvReader.readLine()) != null; i++) {
                if (i == 0) {
                    headers = strCurrentLine.split(",");
                } else {
                    rawRow = strCurrentLine;
                    if (!strCurrentLine.contains(",,")){
                        row = rawRow.split(","); // handle missing values (replace them with white space)
                        int deviceId = Integer.parseInt(row[0]);
                        if (deviceId != lastId) { // Concurrency check
                            if (!uniqueIds.contains(deviceId)) { //Check for possible not continuos increase
                                uniqueIds.add(deviceId);
                            }
                            lastId = deviceId;
                            startIndex.add(i - 1); //0th row is header
                        }
                        values.add(row);
                    } else {
                        count++;
                    }
                }
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {
                if (csvReader != null)
                    csvReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        ArrayList<MyThread> threads = new ArrayList();
        for (int i=0; i< uniqueIds.size()-1; i++){
            threads.add(new MyThread(""+uniqueIds.get(i), values.subList(startIndex.get(i), startIndex.get(i+1)), headers));
        }

    }

    public static void main(String[] args) throws IOException {
        long startTime = System.nanoTime();
        readCSV();
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        System.out.println("Execution time in milliseconds : " + timeElapsed / 1000000);

    }


}
